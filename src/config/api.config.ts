export const API = {
    // backend: '/saga_backend/api', 
    backend: 'http://172.21.213.227:9997/api', 
    backend_user:'http://172.21.213.227:9997/user',
    backend_data_resource:'http://172.21.213.227:8083/dataResource',
    backend_file:'http://172.21.213.227:8083/file',
    // backend_data_resource:'http://localhost:8083/dataResource',
    // backend_file:'http://localhost:8083/file',
    backend_model_container:'http://172.21.213.227:8060/geodata',
    backend_geoserver:'http://172.21.213.227:8080/geoserver',
}